# IOSerial library for C++


ioserial is a C++ stream library for RS-232 ports. It integrates nicely with the C++ STL streaming library and provides buffered I/O of serial ports.

## FEATURES

- Easy to use STL-like library.
- Header-only library.
- Compatible with the STL streaming classes for formatted I/O.
- Buffered I/O for high performance.


## REQUIREMENTS

* A C++17 compiler
* Hardware RS-232 serial port

## USAGE

Just `#include "ioserial/ioserial.hh"`.
Refer to `REFERENCE.md` for more details.

### Testing ioserial

If you want to run the tests, you will need two serial ports connected
by a null-modem cable. Build with

``` bash
$ mkdir testbuild
$ cd testbuild
$ cmake ..
$ make
```
and run with
``` bash
$ ./ioserial.test /dev/ttyUSB0 /dev/ttyUSB1
```
where `ttyUSB0` and `ttyUSB1` are the device files of the two distinct serial ports.
In Windows use `COM1` and `COM2` instead (change to the actual serial devices in your system).


## LICENSE

ioserial is distributed under the terms of the General Public License, version 3.

## TODO

- Serial port auto-detection
- Optimizations in handling the internal buffer: Do not move the data with overflow/underflow so often.
- Use exceptions instead of std::cerr

